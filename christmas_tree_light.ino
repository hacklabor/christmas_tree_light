#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <ArduinoOTA.h>
#include <Adafruit_NeoPixel.h>

int brightness = 255;
boolean active = true;
int neopixelspeed = 100;

extern "C" {
#include "user_interface.h" // to set ESP hostname
}

#include "config.h"

long lastcycle = 0;
long lastcolorswitch = 0;
int color = 1;
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, NeoPIN, NEO_GRB + NEO_KHZ800);
int j = 0;
int q = 0;
int i = 0;

// WiFi
WiFiClient espClient;
PubSubClient client(espClient);

void setup() {
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, mqtt_server_port);
  client.setCallback(callback);
}

void setup_wifi() {
  WiFi.mode(WIFI_STA); // set WiFi mode, no AP
  WiFi.begin(ssid, password);

  String WiFiConnectionStringOne = "Connecting to WiFi network ";
  String WiFiConnectionStringTwo = "...";
  String WiFiConnectionStringComplete = WiFiConnectionStringOne + "'" + ssid + "'" + WiFiConnectionStringTwo;
  Serial.println();
  Serial.print(WiFiConnectionStringComplete);

  // check if connected to WiFi network
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println(" success!");

  wifi_station_set_hostname(espHostname); // set ESP hostname

  //OTA
  ArduinoOTA.setHostname(espHostname);
  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();

  Serial.println();
  Serial.print("Hostname:    ");
  Serial.println(WiFi.hostname());
  Serial.print("IP address:  ");
  Serial.println(WiFi.localIP());
  Serial.print("MAC address: ");
  Serial.println(WiFi.macAddress());
  Serial.println();

  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  String color("#");
  String command;
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
    command = command + String((char)payload[i]);
  }

  Serial.println();

  if (command.toInt() > 0) {
    neopixelspeed = 200 - command.toInt();
  }
  if (command == "on") {
    active = true;
  } else if (command == "off") {
    active = false;
    for (int i = 0; i < NUM_LEDS; i++) {
      for (int i = 0; i < strip.numPixels(); i++) {
        strip.setPixelColor(i, 0, 0, 0);
      }
      strip.show();
    }
  }// finding Color(hex)payload
  if ((char)payload[0] == '#') {
    active = false;
    // setting color
    for (int i = 0; i < strip.numPixels(); i++) {
      strip.setPixelColor(i, 0, 0, 0);
    }
    strip.show();
    setNeoColor(command);
  } else if ((char)payload[0] == '%') {
    command.remove(0, 1);
    strip.setBrightness(command.toInt());
    Serial.println(command);
  }
}


void setup_mqtt() {
  String MQTTConnectionStringOne = "Connecting to MQTT server ";
  String MQTTConnectionStringTwo = "...";
  String MQTTConnectionStringComplete = MQTTConnectionStringOne + "'" + mqtt_server + "'" + MQTTConnectionStringTwo;
  Serial.println();
  Serial.print(MQTTConnectionStringComplete);

  // check if connected to MQTT server
  while (!client.connected()) {
    if (!client.connect(willClientID, willTopic, willQoS, willRetain, willOffMessage)) {
      delay(1000);
      Serial.print(".");
    } else {
      Serial.println(" success!");
      // Once connected, publish an announcement...
      client.publish(willTopic, willOnMessage, willRetain);
      // ... and resubscribe
      client.subscribe(mainTopic);
    }
  }
}

void loop() {
  if (WiFi.status() != WL_CONNECTED) {
    setup_wifi();
  }
  if (!client.connected()) {
    setup_mqtt();
  }
  client.loop();
  ArduinoOTA.handle();
  long now = millis();
  if (active) {
    if (now - lastcolorswitch < 5000) {
      if (color == 1) {
        theaterChase(strip.Color(127, 0, 0)); // Red
      } else if (color == 2) {
        theaterChase(strip.Color(0, 127, 0)); // Green
      } else if (color == 3) {
        theaterChase(strip.Color(0, 0, 255)); // Blue
      } else if (color == 4) {
        colorWipe(strip.Color(255, 0, 0), 50); // Red
      } else if (color == 5) {
        colorWipe(strip.Color(0, 255, 0), 50); // Green
      } else if (color == 6) {
        colorWipe(strip.Color(0, 0, 255), 50); // Blue
      }
    } else {
      i = 0;
      lastcolorswitch = now;
      color = color + 1;
      if (color == 7) {
        color = 1;
      }
    }
  }
}
// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  if (i < strip.numPixels()) {
    i++;
    strip.setPixelColor(i, c);
    strip.show();
    delay(wait);
  }

}

//Theatre-style crawling lights.
void theaterChase(uint32_t c) {
  long now = millis();
  if (now - lastcycle > neopixelspeed) {
    lastcycle = now;
    if (j < 256) {
      j++;
      if (q < 3) {
        q++;
        for (int i = 0; i < strip.numPixels(); i = i + 3) {
          strip.setPixelColor(i + q, c); //turn every third pixel on
        }
        strip.show();

        for (int i = 0; i < strip.numPixels(); i = i + 3) {
          strip.setPixelColor(i + q, 0);      //turn every third pixel off
        }
      }
      if (j == 256)
        j = 0;

      if (q == 3)
        q = 0;
    }

  }
}
void setNeoColor(String value) {
  int number = (int) strtol( &value[1], NULL, 16);

  int r = number >> 16;
  int g = number >> 8 & 0xFF;
  int b = number & 0xFF;

  Serial.print("RGB: ");
  Serial.print(r, DEC);
  Serial.print(" ");
  Serial.print(g, DEC);
  Serial.print(" ");
  Serial.print(b, DEC);
  Serial.println(" ");

  for (int i = 0; i < NUM_LEDS; i++) {
    strip.setPixelColor(i, strip.Color( r, g, b ) );
  }
  strip.show();

}
